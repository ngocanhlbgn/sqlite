package com.example.sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    StudentHelper studentHelper;
    ListView lv;
    ArrayList<NoiDung> noiDungArrayList;
    NoiDungAdapter adapter;

    EditText edtMaSV, edtHoTen, edtGioiTinh;
    Button btnThem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        anhXa();

        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String maSV = edtMaSV.getText().toString().trim();
                String hoTen = edtHoTen.getText().toString().trim();
                String gioiTinh = edtGioiTinh.getText().toString().trim();

                if(TextUtils.isEmpty(maSV) && TextUtils.isEmpty(hoTen) && TextUtils.isEmpty(gioiTinh)) {
                    Toast.makeText(MainActivity.this, "Vui lòng nhập dữ liệu", Toast.LENGTH_SHORT).show();
                    return;
                }
                studentHelper.QueryData("INSERT INTO NoiDung VALUES (null, ' "+ maSV +" "+hoTen+" "+gioiTinh+" ')");
                actionGetData();
            }
        });

        noiDungArrayList = new ArrayList<>();
        adapter = new NoiDungAdapter(this, R.layout.rows, noiDungArrayList);
        lv.setAdapter(adapter);

        //create databse
        studentHelper = new StudentHelper(this, "ListSV", null, 1);

        //create table

        studentHelper.QueryData("CREATE TABLE IF NOT EXISTS ListSV(Id INTEGER PRIMARY KEY AUTOINCREMENT, MaSV VARCHAR(200), HoTen VARCHAR(200), GioiTinh VARCHAR(200) )");

        //show
        actionGetData();

    }

    private void actionGetData() {
        Cursor dataSV = studentHelper.GetData("SELECT * FROM ListSV");
        while(dataSV.moveToNext()) {
            int id = dataSV.getInt(0);
            String maSV = dataSV.getString(1);
            String hoTen = dataSV.getString(1);
            String gioiTinh = dataSV.getString(1);
            noiDungArrayList.add(new NoiDung(id, maSV, hoTen, gioiTinh));
        }
        adapter.notifyDataSetChanged();

    }

    private void anhXa() {

        edtMaSV = findViewById(R.id.edtMaSV);
        edtHoTen = findViewById(R.id.edtHoTen);
        edtGioiTinh = findViewById(R.id.edtGioiTinh);
        lv = findViewById(R.id.lv_NoiDung);
    }
}