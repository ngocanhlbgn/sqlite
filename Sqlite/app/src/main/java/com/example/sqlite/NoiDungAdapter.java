package com.example.sqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class NoiDungAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<NoiDung> noiDungList;

    public NoiDungAdapter(Context context, int layout, List<NoiDung> noiDungList) {
        this.context = context;
        this.layout = layout;
        this.noiDungList = noiDungList;
    }

    @Override
    public int getCount() {
        return noiDungList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView tvMaSV, tvHoTen, tvGioiTinh;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, null);

            viewHolder.tvMaSV = (TextView) convertView.findViewById(R.id.edtMaSV);
            viewHolder.tvHoTen = (TextView) convertView.findViewById(R.id.edtHoTen);
            viewHolder.tvGioiTinh = (TextView) convertView.findViewById(R.id.edtGioiTinh);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        NoiDung noiDung = noiDungList.get(position);
        viewHolder.tvMaSV.setText(noiDung.getMaSV());
        viewHolder.tvHoTen.setText(noiDung.getHoTen());
        viewHolder.tvGioiTinh.setText(noiDung.getGioiTinh());
        return convertView;
    }
}
